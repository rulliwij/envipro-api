<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends Auth_Api_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        $data = array();
        $bek = $this->get_data_dashboard('bek');
        $data['device'][] = $bek;
        $imm = $this->get_data_dashboard('imm');
        $data['device'][] = $imm;
        $ktd = $this->get_data_dashboard('ktd');
        $data['device'][] = $ktd;
        $tcm = $this->get_data_dashboard('tcm');
        $data['device'][]= $tcm;
        $this->createResponse(REST_Controller::HTTP_OK, 'Data Dashboard', $data);
    }

    public function get_data_dashboard($callsign)
    {
        $table = "sparing";
        $sql = "
            SELECT sparing_id, device_serial_number, sparing_callsign, device_user_id, device_name, device_latitude, device_longitude, sparing_create_datetime, sparing_ph, sparing_cod, sparing_tss, sparing_nh3n, sparing_debit
            FROM device
            JOIN sparing ON sparing_station_id = device_name
            WHERE sparing_callsign = 'callsign'
            ORDER BY sparing_id DESC
        ";
        return $this->db->query($sql)->row();
    }
}