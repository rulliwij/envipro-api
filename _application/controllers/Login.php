<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Base_Api_Controller {


    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }

    public function index_post()
    {
        $this->form_validation->set_rules('username', '<b>Username</b>', 'trim|htmlspecialchars|required');
        $this->form_validation->set_rules('password', '<b>Password</b>', 'trim|htmlspecialchars|required');
        if ($this->form_validation->run() == false) {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, validation_errors());
        } else {
            $username = addslashes($this->post('username'));
            $password = addslashes($this->post('password'));
            $sql = "SELECT user_id, user_username AS username, user_password AS password, user_email AS email FROM user WHERE user_username = '$username' 
            AND user_status = 'active' AND user_is_verified = 1";
            $user = $this->db->query($sql)->row();
            if (empty($user)) {
                $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'Akun tidak ditemukan atau akun anda sudah dinontifkan. Silakan hubungi admin.');
            }
            if (!password_verify($password, $user->password)) {
                $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'Password yang anda masukkan salah.');
            }
            $token = $this->generateToken($user->email, $this->input->ip_address());
            $datetime = date("Y-m-d H:i:s");
            $arr_insert = array(
                'token_value' => $token,
                'token_create_datetime' => $datetime,
                'token_update_datetime' => $datetime,
                'token_user_id' => $user->user_id,
            );
            $insert_token = $this->db->insert('token', $arr_insert);
            if (!$insert_token) {
                $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'Gagal update token. Silakan login ulang.');
            }
            $data_user = $this->get_user($username);
            $data_user->token = $token;
            $this->createResponse(REST_Controller::HTTP_OK, 'Login Berhasil.', $data_user);
        }
    }

    private function get_user($username){
        $sql = "SELECT * FROM user WHERE user_username = '$username'";
        return $this->db->query($sql)->row();
    }
}