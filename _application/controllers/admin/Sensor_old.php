<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sensor_old extends Auth_Api_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        $callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $start = $this->get('start') ?? date('Y-m-d H:i:s');
        $end = $this->get('end') ?? date('Y-m-d H:i:s');
        $table = 'sparing_'.$callsign;
        $sql = "
            SELECT id, idstasiun, waktu, debit, tss, ph
            FROM $table
            ORDER BY id DESC
            LIMIT 1
        ";
        $data[$callsign] = $this->db->query($sql)->row();
        $table = 'sparing_'.$callsign;
        $table_and_join = "FROM ".$table;
        $where_detail = "date(waktu) BETWEEN '$start' AND '$end'";

        $arr_show = array(
            'id',
            'idstasiun',
            'waktu',
            'ph',
            'tss',
            'debit',
        );
        $arr_search = array(
            'idstasiun',
            'waktu',
            'ph',
            'tss',
            'debit',
        );

        $data['list'] = generate_data_query($this->get(), $table_and_join, $where_detail, $arr_show, $arr_search);

        $this->createResponse(REST_Controller::HTTP_OK, 'Data Sensor '.strtoupper($callsign), $data);
    }
}