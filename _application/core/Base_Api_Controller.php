<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Base_Api_Controller extends REST_Controller {


    private $APP_NAME = 'envipro';

    public function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization, Token");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        if (empty($_POST) && $_SERVER['REQUEST_METHOD'] === 'POST') {
            $_POST = json_decode(file_get_contents('php://input'), true);
        }
        if (empty($_PUT) && $_SERVER['REQUEST_METHOD'] === 'PUT') {
            $_PUT = json_decode(file_get_contents('php://input'), true);
        }
    }

    public function createResponse($status = REST_Controller::HTTP_OK, $message = 'Success', $data = array())
    {
        header("Content-Type: application/json");
        header("HTTP/1.1 200 OK");
        echo json_encode(array(
            'status'    => $status,
            'message'   => $message,
            'results'      => (object)$data
        ));
        exit;
    }

    public function unauthorized()
    {
        header("Content-Type: application/json");
        header("HTTP/1.1 401 Unauthorized");
        header("WWW-Authenticate: Bearer realm=\"app\"");
        echo json_encode(array(
            "status"    => REST_Controller::HTTP_UNAUTHORIZED,
            "message"   => 'Unauthorized',
        ));
        exit;
    }


    public function getHeaderToken() {
        $pattern = "/(^|&)(?P<token>[a-z0-9]+\+[a-z0-9]+)/";
        $token = $this->input->get_request_header('token');
        if (preg_match($pattern, $token, $result)) {
            return $result[0];
        } else {
            return '';
        }
    }

    public function getUser(){
        $token = $this->getHeaderToken();
        $users = [];
        $user = $this->getTokenUser($token);
        if(!empty($user)){
            $users = $user;
        }
        return $users;
    }



    public function getTokenUser($token) {
        $userArr = array();

        $sql = "
            SELECT 
            token_user_id,
            user_name AS name,
            user_email AS email,
            user_username AS username,
            user_password AS password,
            user_level AS level,
            user_parent AS parent,
            user_company AS company,
            user_create_datetime AS create_datetime,
            user_update_datetime AS update_datetime,
            user_status AS status,
            user_is_verified AS is_verified
            FROM token
            JOIN user ON token_user_id = user_id
            WHERE token_value = '" . $token . "' AND token_is_active = '1'
        ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $userArr = $query->row();
        }

        return $userArr;
    }

    public function generateToken($userEmail, $userIp) {
        return sha1($userEmail.microtime()).'+'.sha1($this->APP_NAME.microtime().$userIp);
    }

    public function generateHash($userEmail, $userIp) {
        return sha1($userEmail.microtime()).'SIK'.sha1($this->APP_NAME.microtime().$userIp);
    }

    // private function checkTokenExpired($token, $expiredTime, $authID) {
    //     $now = time();
    //     if ($now > $expiredTime) {
    //         $this->db->where('user_auth_token', $token);
    //         $this->db->delete('user_auth');
    //         return true;
    //     } else {
    //         $extraTime = strtotime("240 minutes", $now);
    //         $update = array(
    //             'user_auth_expired' => $extraTime
    //         );
    //         $this->db->where('user_auth_id', $authID);
    //         $this->db->update('user_auth', $update);
    //         return false;
    //     }
    // }
}
