<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Public_Api_Controller extends Base_Api_Controller
{

    protected $account;
    
    public function __construct()
    {
        parent::__construct();
        $this->account = $this->getUser();
    }
}
