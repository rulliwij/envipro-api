<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sensor extends Auth_Api_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        $callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $start = $this->get('start') ?? date('Y-m-d H:i:s');
        $end = $this->get('end') ?? date('Y-m-d H:i:s');
        $table = 'sparing_'.$callsign;
        $sql = "
            SELECT sparing_id, sparing_station_id, sparing_ph, sparing_tss, sparing_debit, sparing_callsign, sparing_create_datetime, sparing_update_datetime
            FROM sparing
            ORDER BY sparing_id DESC
            LIMIT 1
        ";
        $data[$callsign] = $this->db->query($sql)->row();
        // $table = 'sparing_'.$callsign;
        $table_and_join = "FROM sparing";
        $where_detail = "date(sparing_create_datetime) BETWEEN '$start' AND '$end' AND sparing_callsign = '$callsign'";

        $arr_show = array(
            'sparing_id',
            'sparing_station_id',
            'sparing_ph',
            'sparing_tss',
            'sparing_debit',
            'sparing_create_datetime'
        );
        $arr_search = array(
            'sparing_station_id',
            'sparing_ph',
            'sparing_tss',
            'sparing_debit',
            'sparing_create_datetime'
        );

        $data['list'] = generate_data_query($this->get(), $table_and_join, $where_detail, $arr_show, $arr_search);

        $this->createResponse(REST_Controller::HTTP_OK, 'Data Sensor '.strtoupper($callsign), $data);
    }
}