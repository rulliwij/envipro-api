<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Device extends Auth_Api_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list_get()
    {
        $callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $table = 'sparing_'.$callsign;
        $sql = "
            SELECT sparing_id, sparing_station_id, sparing_ph, sparing_tss, sparing_debit, sparing_callsign, sparing_create_datetime, sparing_update_datetime
            FROM sparing
            WHERE sparing_callsign = '$callsign' AND sparing_status = 'active'
            ORDER BY sparing_id DESC
            LIMIT 1
        ";
        $data[$callsign] = $this->db->query($sql)->row();

        $table_and_join = "FROM device";
        $where_detail = "device_status = 'active'";

        $arr_show = array(
            'device_id',
            'device_name',
            'device_serial_number',
            'device_callsign',
            'device_user_id',
            'device_latitude',
            'device_longitude',
            'device_create_datetime',
            'device_update_datetime',
            'device_status',
        );

        $arr_search = array(
            'device_id',
            'device_name',
            'device_serial_number',
            'device_callsign',
            'device_user_id',
            'device_latitude',
            'device_longitude',
            'device_create_datetime',
            'device_update_datetime',
            'device_status',
        );

        $data['list'] = generate_data_query($this->get(), $table_and_join, $where_detail, $arr_show, $arr_search);

        $this->createResponse(REST_Controller::HTTP_OK, 'Data Device '.strtoupper($callsign), $data);
    }

    public function all_get()
    {
        $sql ="SELECT * FROM device WHERE device_status = 'active'";
        $data['data'] = $this->db->query($sql)->result_array();
        $this->createResponse(REST_Controller::HTTP_OK, 'Data Device ', $data);
    }
}