<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maps_old extends Auth_Api_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
    	$callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $table = 'sparing_'.$callsign;
        $sql = "
            SELECT id, idstasiun, waktu, debit, tss, ph
            FROM $table
            ORDER BY id DESC
            LIMIT 1
        ";
        $data[$callsign] = $this->db->query($sql)->row();
        $idstasiun = $data[$callsign]->idstasiun;
        $sql_maps = "
        	SELECT id, sn, callsign, nama, user, latitude, longitude
        	FROM list_device
        	WHERE nama = '$idstasiun'
        ";
        $data['maps'] = $this->db->query($sql_maps)->row();
        $this->createResponse(REST_Controller::HTTP_OK, 'Data Maps '.strtoupper($callsign), $data);
    }

}