<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maps extends Auth_Api_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
    	$callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $table = 'sparing_'.$callsign;
        $sql = "
            SELECT  sparing_id, sparing_station_id, sparing_ph, sparing_tss, sparing_debit, sparing_callsign, sparing_create_datetime, sparing_update_datetime
            FROM sparing
            WHERE sparing_callsign = '$callsign'
            ORDER BY sparing_id DESC
            LIMIT 1
        ";
        $data[$callsign] = $this->db->query($sql)->row();
        $idstasiun = "";
        if (!empty($data[$callsign])) {
            $idstasiun = $data[$callsign]->sparing_station_id;
        }
        $sql_maps = "
            SELECT device_id,
            device_name,
            device_serial_number,
            device_callsign,
            device_user_id,
            device_latitude,
            device_longitude,
            device_create_datetime,
            device_update_datetime,
            device_status
        	FROM device
        	WHERE device_name = '$idstasiun'
        ";
        $data['maps'] = $this->db->query($sql_maps)->row();
        $this->createResponse(REST_Controller::HTTP_OK, 'Data Maps '.strtoupper($callsign), $data);
    }

}