<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Chart_old extends Auth_Api_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
    	$callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $start = $this->get('start') ?? date('Y-m-d H:i:s');
        $end = $this->get('end') ?? date('Y-m-d H:i:s');
        $table = 'sparing_'.$callsign;
    	$sql = "
    		SELECT id, idstasiun, date(waktu) AS tanggal, waktu, time(waktu) AS jam, debit, tss, ph
    		FROM $table
            WHERE (date(waktu) BETWEEN '$start' AND '$end')
            ORDER BY id DESC
            LIMIT 20
    	";
    	$data = $this->db->query($sql)->result_array();
        $arr_result = array();
        if (!empty($data)) {
            foreach ($data as $key => $row) {
                $arr_result['debit'][] = array(
                    'tanggal' => $row['tanggal'],
                    'jam' => $row['jam'],
                    'debit' => $row['debit'],
                );
                $arr_result['tss'][] = array(
                    'tanggal' => $row['tanggal'],
                    'jam' => $row['jam'],
                    'tss' => $row['tss'],
                );
                $arr_result['ph'][] = array(
                    'tanggal' => $row['tanggal'],
                    'jam' => $row['jam'],
                    'ph' => $row['ph'],
                );
            }
        }
        $this->createResponse(REST_Controller::HTTP_OK, 'Data Chart '.strtoupper($callsign), $arr_result);
    }

    public function device_get()
    {
        $sql ="SELECT * FROM list_device WHERE callsign <> ''";
        $data = $this->db->query($sql)->result_array();
        $arr_result = array();
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $arr_result['data'][] = $value;
            }
        }
        $this->createResponse(REST_Controller::HTTP_OK, 'Data Device ', $arr_result);
    }

    public function list_get()
    {
        $device = $this->get('device');
        if (!isset($device) || $device == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'device tidak boleh kosong.');
        }
        $type = $this->get('type');
        if (!isset($type) || $type == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'type tidak boleh kosong.');
        }
        $start = $this->get('start') ?? date('Y-m-d H:i:s');
        $end = $this->get('end') ?? date('Y-m-d H:i:s');
        $data[$device] = $device;
        $table = 'sparing_'.$device;
        $table_and_join = "FROM ".$table;
        $where_detail = "date(waktu) BETWEEN '$start' AND '$end'";

        $arr_show = array(
            'id',
            'idstasiun',
            'date(waktu) AS tanggal',
            'waktu', 
            'time(waktu) AS jam',
            $type
        );
        $arr_search = array(
            'idstasiun',
            'date(waktu) AS tanggal',
            'waktu',
            'time(waktu) AS jam',
        );
        $list = generate_data_query($this->get(), $table_and_join, $where_detail, $arr_show, $arr_search);
        $data['list'] = $list;
        $this->createResponse(REST_Controller::HTTP_OK, 'Data Chart '.strtoupper($device).' Datatable '.strtoupper($type), $data);
    }

    public function pdf_get()
    {
        $this->load->library('Pdf');
        $callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $type = $this->get('type');
        if (!isset($type) || $type == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'type tidak boleh kosong.');
        }
        $start = $this->get('start') ?? date('Y-m-d H:i:s');
        $end = $this->get('end') ?? date('Y-m-d H:i:s');
        $data[$callsign] = $callsign;
        $table = 'sparing_'.$callsign;
        $sql = "
            SELECT id, idstasiun, date(waktu) AS tanggal, time(waktu) AS jam, $type
            FROM $table
            WHERE date(waktu) BETWEEN '$start' AND '$end'
            ORDER BY id ASC
            LIMIT 1000
        ";
        $data['list'] = $this->db->query($sql)->result();
        if ($type == 'tss') {
            $type = "Turbidity";
        }
        $arr_data = array();
        $arr_data['title'] = "Data ".strtoupper($callsign). " ".strtoupper($type);
        $arr_data['tanggal'] = convert_date($start,'', '', 'id')." s/d ".convert_date($end,'', '', 'id');
        $arr_data['type'] = $type;
        $arr_data['data'] = $data['list'];
        $html = $this->load->view('admin/chart_pdf_view', $arr_data, true);
        ob_start();
        $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->setPageFormat('A4', 'P');
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('ENVIPRO');
        $pdf->SetTitle('Title');
        $pdf->SetSubject('subject');

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->AddPage();

        $pdf->SetFont("Calibri", "", 10);
        $pdf->writeHTML($html, true, false, false, false, '');

        $pdf->lastPage();
        $filename = url_title($arr_data['title']).'-'.$arr_data['tanggal'].'.pdf';
        ob_end_clean();
        $pdf->Output(FCPATH . '/assets/file/' . $filename, "I");
    }

    public function excel_get()
    {
        $callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $type = $this->get('type');
        if (!isset($type) || $type == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'type tidak boleh kosong.');
        }
        $start = $this->get('start') ?? date('Y-m-d H:i:s');
        $end = $this->get('end') ?? date('Y-m-d H:i:s');
        $table = 'sparing_'.$callsign;
        $sql = "
            SELECT id, idstasiun, date(waktu) AS tanggal, time(waktu) AS jam, $type
            FROM $table
            WHERE date(waktu) BETWEEN '$start' AND '$end'
            ORDER BY id ASC
            LIMIT 10
        ";
        $data['title'] = "Data ".strtoupper($callsign). " ".strtoupper($type);
        $data['tanggal'] = convert_date($start,'', '', 'id')." s/d ".convert_date($end,'', '', 'id');
        $data['export_date'] = convert_date(date('Y-m-d H:i:s'), '', '', 'id');
        $data['type'] = $type;
        $data['list'] = $this->db->query($sql)->result();
        if ($type == 'tss') {
            $type = "Turbidity";
        }
        $this->export_excel($data);
    }

    public function export_excel($data)
    {
        if (!empty($data)) {
            $first_column = $cell_column = 'A';
            $cell_row = $first_row = 1;
            $sheet = new Spreadsheet();

            $sheet->getProperties()->setCreator('ENVIPRO')
                ->setLastModifiedBy('Envipro')
                ->setTitle("Data Chart ".$data['type'])
                ->setSubject("Data Chart ".$data['type'])
                ->setDescription("")
                ->setKeywords("Data Chart ".$data['type']);

            $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(12);
            $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, strtoupper($data['title']. " ".$data['tanggal']));

            $cell_row++;
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(10);
            $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, 'Tanggal Export : ' .$data['export_date']);
            $cell_row++;
            $cell_row++;
            $cell_row++;

            $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
            $sheet->getActiveSheet()->getColumnDimension($cell_column)->setWidth(5);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(10);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal('center');
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setVertical('center');
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, 'NO');
            $cell_column++;

            $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
            $sheet->getActiveSheet()->getColumnDimension($cell_column)->setWidth(30);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(10);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal('center');
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setVertical('center');
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, 'TANGGAL');
            $cell_column++;

            $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
            $sheet->getActiveSheet()->getColumnDimension($cell_column)->setWidth(20);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(10);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal('center');
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setVertical('center');
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, 'WAKTU');
            $cell_column++;

            $sheet->getActiveSheet()->getRowDimension($cell_row)->setRowHeight(20);
            $sheet->getActiveSheet()->getColumnDimension($cell_column)->setWidth(20);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setBold(true);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getFont()->setSize(10);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setHorizontal('center');
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getAlignment()->setVertical('center');
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle($cell_column . $cell_row)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->setCellValue($cell_column . $cell_row, strtoupper($data['type']));
            $cell_row++;

            $cell_column = 'A';
            $cell_row = 6;
            $no = 1;
            $type = $data['type'];
            $total_row = count($data['list']);
            foreach ($data['list'] as $key => $row) {
                $sheet->setActiveSheetIndex(0)->setCellValue('A'.$cell_row, $no);
                $sheet->getActiveSheet()->getStyle('A'.$cell_row)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getActiveSheet()->getStyle('A' . $cell_row)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getActiveSheet()->getStyle('A'.$cell_row)->getAlignment()->setHorizontal('center');
                $sheet->getActiveSheet()->getStyle('A'.$cell_row)->getAlignment()->setVertical('center');

                $sheet->setActiveSheetIndex(0)->setCellValue('B'.$cell_row, convert_date($row->tanggal, '', '', 'id'));
                $sheet->getActiveSheet()->getStyle('B'.$cell_row)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getActiveSheet()->getStyle('B' . $cell_row)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getActiveSheet()->getStyle('B'.$cell_row)->getAlignment()->setHorizontal('center');
                $sheet->getActiveSheet()->getStyle('B'.$cell_row)->getAlignment()->setVertical('center');

                $sheet->setActiveSheetIndex(0)->setCellValue('C'.$cell_row, $row->jam);
                $sheet->getActiveSheet()->getStyle('C'.$cell_row)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getActiveSheet()->getStyle('C' . $cell_row)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getActiveSheet()->getStyle('C'.$cell_row)->getAlignment()->setHorizontal('center');
                $sheet->getActiveSheet()->getStyle('C'.$cell_row)->getAlignment()->setVertical('center');

                $sheet->setActiveSheetIndex(0)->setCellValue('D'.$cell_row, $row->$type);
                $sheet->getActiveSheet()->getStyle('D'.$cell_row)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getActiveSheet()->getStyle('D' . $cell_row)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                $sheet->getActiveSheet()->getStyle('D'.$cell_row)->getAlignment()->setHorizontal('center');
                $sheet->getActiveSheet()->getStyle('D'.$cell_row)->getAlignment()->setVertical('center');

                if ($no >= $total_row) {
                    break;
                }
                $no++;
                $cell_row++;
            }
            $sheet->getActiveSheet()->getStyle('A' . $cell_row)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle('B' . $cell_row)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle('C' . $cell_row)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getActiveSheet()->getStyle('D' . $cell_row)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            
            $writer = new Xlsx($sheet); // instantiate Xlsx
            $filename = 'export-'.$data['export_date']; // set filename for excel file to be exported
     
            header('Content-Type: application/vnd.ms-excel'); // generate excel file
            header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
            header('Cache-Control: max-age=0');
            
            $writer->save('php://output');  // download file 
        }
    }
}