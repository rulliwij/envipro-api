<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Device_old extends Auth_Api_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list_get()
    {
        $callsign = $this->get('callsign');
        if (!isset($callsign) || $callsign == '') {
            $this->createResponse(REST_Controller::HTTP_BAD_REQUEST, 'callsign tidak boleh kosong.');
        }
        $table = 'sparing_'.$callsign;
        $sql = "
            SELECT id, idstasiun, waktu, debit, tss, ph
            FROM $table
            ORDER BY id DESC
            LIMIT 1
        ";
        $data[$callsign] = $this->db->query($sql)->row();

        $table_and_join = "FROM list_device";
        $where_detail = "callsign <> ''";

        $arr_show = array(
            'id',
            'sn',
            'callsign',
            'nama',
            'user',
            'latitude',
            'longitude',
        );

        $arr_search = array(
            'sn',
            'callsign',
            'nama',
            'user',
            'latitude',
            'longitude',
        );

        $data['list'] = generate_data_query($this->get(), $table_and_join, $where_detail, $arr_show, $arr_search);

        $this->createResponse(REST_Controller::HTTP_OK, 'Data Device '.strtoupper($callsign), $data);
    }

    public function all_get()
    {
        $sql ="SELECT * FROM list_device WHERE callsign <> ''";
        $data['data'] = $this->db->query($sql)->result_array();
        $this->createResponse(REST_Controller::HTTP_OK, 'Data Device ', $data);
    }
}