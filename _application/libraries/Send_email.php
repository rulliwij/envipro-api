<?php


class Send_email
{

    private $curl = null;


    public function __construct()
    {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_URL, EMAIL_URL);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($this->curl, CURLOPT_USERPWD, EMAIL_KEY);
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'POST');
    }

    public function email_verification($data)
    {
        $mailData = array();
        $mailData['from'] = EMAIL_FROM;
        $mailData['to'] = $data['to'];
        $mailData['subject'] = "Hi, {$data['name']}. Silahkan Verifikasi akun G-Production Anda";
        $content_html = '
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style=" background: white; margin-top: -15px; padding-top: 25px;">
                <tr>
                <td align="center" style="padding: 10px 0 10px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                    <img src="https://keluargabidankita.com/wp-upload/email/verifikasi-email.png" width="100" height="100" style="display: block; object-fit: contain;" />
                </td>
                </tr>
                <tr>
                <td bgcolor="#ffffff" style="padding: 10px 30px 0px 30px;line-height: 1.5">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align:center;">
                    <tr>
                        <td style="font-weight: 600; color: black; font-family: Arial, sans-serif; font-size: 24px; padding-bottom: 15px;">
                        Hi, ' . $data['name'] . '
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 16px;font-weight: 600; padding-bottom: 15px; line-height: 1.5; width: 600px; margin-left: auto; margin-right: auto;">
                        Terima kasih telah bergabung dengan G-Production, silahkan konfirmasi email Anda
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <a href="' . $data['link'] . '" style="border:0; background:#FF3177; border-radius: 25px; padding: 10px 20px; text-decoration: none; display: inline-block; font-size: 17px; color: white; font-family: Arial, sans-serif; margin-bottom: 25px;">
                            Konfirmasi Sekarang
                        </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 16px; font-weight: 600; padding-bottom: 15px;">
                        atau klik link dibawah ini
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: Arial, sans-serif; font-size: 13px; font-weight: 600; padding-bottom: 15px; padding-top: 0px;">
                        <a 
                            style="color: #FF3177;"
                            href="' . $data['link'] . '">
                            ' . $data['link'] . '
                        </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 13px; font-weight: 600; padding-top: 0px; line-height: 1.5;">
                        Anda bisa mengabaikan email ini jika Anda tidak merasa melakukan <br> pendaftaran di <span style="color: #000000c7;"> G-Production</span>
                        </td>
                    </tr>
                    </table>
                </td>
                </tr>
            </table>
        ';
        $mailData['html'] = $this->template_email($content_html);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $mailData);
        $result = curl_exec($this->curl);
        curl_close($this->curl);
        return $result;
    }

    public function registration_success($data)
    {
        $mailData = array();
        $mailData['from'] = EMAIL_FROM;
        $mailData['to'] = $data['to'];
        $mailData['subject'] = "Hi, {$data['name']}. Registrasi akun G-Production Anda telah berhasil.";
        $content_html = '
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style=" background: white; margin-top: -15px; padding-top: 25px;">
                <tr>
                <td align="center" style="padding: 10px 0 10px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                    <img src="https://keluargabidankita.com/wp-upload/email/registrasi-berhasil.png" width="100" height="100" style="display: block; object-fit: contain;" />
                </td>
                </tr>
                <tr>
                <td bgcolor="#ffffff" style="padding: 10px 30px 0px 30px;line-height: 1.5">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align:center;">
                    <tr>
                        <td style="font-weight: 600; color: black; font-family: Arial, sans-serif; font-size: 24px; padding-bottom: 15px;">
                        Hi, ' . $data['name'] . '
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 16px;font-weight: 600; padding-bottom: 15px; line-height: 1.5; width: 600px; margin-left: auto; margin-right: auto;">
                        Terima kasih telah bergabung di G-Production. Registrasi telah berhasil dan anda dapat login mengunakan email ' . $data['to'] . '
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <a href="' . $data['link'] . '" style="border:0; background:#FF3177; border-radius: 25px; padding: 10px 20px; text-decoration: none; display: inline-block; font-size: 17px; color: white; font-family: Arial, sans-serif; margin-bottom: 25px;">
                            Login Sekarang
                        </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 16px; font-weight: 600; padding-bottom: 15px;">
                        atau klik link dibawah ini
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: Arial, sans-serif; font-size: 13px; font-weight: 600; padding-bottom: 15px; padding-top: 0px;">
                        <a 
                            style="color: #FF3177;"
                            href="' . $data['link'] . '">
                            ' . $data['link'] . '
                        </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 13px; font-weight: 600; padding-top: 0px; line-height: 1.5;">
                        <span style="color: #000000c7;"> G-Production</span>
                        </td>
                    </tr>
                    </table>
                </td>
                </tr>
            </table>
        ';
        $mailData['html'] = $this->template_email($content_html);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $mailData);
        $result = curl_exec($this->curl);
        curl_close($this->curl);
        return $result;
    }

    public function reset_password($data)
    {
        $mailData = array();
        $mailData['from'] = EMAIL_FROM;
        $mailData['to'] = $data['to'];
        $mailData['subject'] = "Hi, {$data['name']}. Permintaan Reset Password.";
        $content_html = '
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style=" background: white; margin-top: -15px; padding-top: 25px;">
                <tr>
                <td align="center" style="padding: 10px 0 10px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                    <img src="https://keluargabidankita.com/wp-upload/email/reset-password.png" width="100" height="100" style="display: block; object-fit: contain;" />
                </td>
                </tr>
                <tr>
                <td bgcolor="#ffffff" style="padding: 10px 30px 0px 30px;line-height: 1.5">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align:center;">
                    <tr>
                        <td style="font-weight: 600; color: black; font-family: Arial, sans-serif; font-size: 24px; padding-bottom: 15px;">
                        Hi, ' . $data['name'] . '
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 16px;font-weight: 600; padding-bottom: 15px; line-height: 1.5; width: 600px; margin-left: auto; margin-right: auto;">
                        Anda telah melakukan permintaan reset password. Silahkan klik tombol di bawah ini dan ikuti langkah berikutnya. 
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <a href="' . $data['link'] . '" style="border:0; background:#FF3177; border-radius: 25px; padding: 10px 20px; text-decoration: none; display: inline-block; font-size: 17px; color: white; font-family: Arial, sans-serif; margin-bottom: 25px;">
                            Reset Password
                        </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 16px; font-weight: 600; padding-bottom: 15px;">
                        atau klik link dibawah ini
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: Arial, sans-serif; font-size: 13px; font-weight: 600; padding-bottom: 15px; padding-top: 0px;">
                        <a 
                            style="color: #FF3177;"
                            href="' . $data['link'] . '">
                            ' . $data['link'] . '
                        </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 13px; font-weight: 600; padding-top: 0px; line-height: 1.5;">
                        Anda bisa mengabaikan email ini jika Anda tidak merasa melakukan <br> permintaan reset password di <span style="color: #000000c7;"> G-Production</span>
                        </td>
                    </tr>
                    </table>
                </td>
                </tr>
            </table>
        ';
        $mailData['html'] = $this->template_email($content_html);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $mailData);
        $result = curl_exec($this->curl);
        curl_close($this->curl);
        return $result;
    }

    public function add_user_success($data)
    {
        $mailData = array();
        $mailData['from'] = EMAIL_FROM;
        $mailData['to'] = $data['to'];
        $mailData['subject'] = "Hi, {$data['name']}. Pendaftaran akun {$data['role']} G-Production.";

        $content_html = '
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style=" background: white; margin-top: -15px; padding-top: 25px;">
                <tr>
                <td align="center" style="padding: 10px 0 10px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                    <img src="https://keluargabidankita.com/wp-upload/email/reset-password.png" width="100" height="100" style="display: block; object-fit: contain;" />
                </td>
                </tr>
                <tr>
                <td bgcolor="#ffffff" style="padding: 10px 30px 0px 30px;line-height: 1.5">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="text-align:center;">
                    <tr>
                        <td style="font-weight: 600; color: black; font-family: Arial, sans-serif; font-size: 24px; padding-bottom: 15px;">
                        Hi, ' . $data['name'] . '
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 16px;font-weight: 600; padding-bottom: 15px; line-height: 1.5; width: 600px; margin-left: auto; margin-right: auto;">
                        Anda telah didaftarkan menjadi ' . $data['role'] . ' di G-Production. Silahkan klik tombol di bawah ini untuk membuat password baru. 
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <a href="' . $data['link'] . '" style="border:0; background:#FF3177; border-radius: 25px; padding: 10px 20px; text-decoration: none; display: inline-block; font-size: 17px; color: white; font-family: Arial, sans-serif; margin-bottom: 25px;">
                            Buat Password
                        </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 16px; font-weight: 600; padding-bottom: 15px;">
                        atau klik link dibawah ini
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: Arial, sans-serif; font-size: 13px; font-weight: 600; padding-bottom: 15px; padding-top: 0px;">
                        <a 
                            style="color: #FF3177;"
                            href="' . $data['link'] . '">
                            ' . $data['link'] . '
                        </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #0000007a; font-family: Arial, sans-serif; font-size: 13px; font-weight: 600; padding-top: 0px; line-height: 1.5;">
                        Anda bisa mengabaikan email ini jika Anda tidak merasa melakukan <br> pendaftaran di <span style="color: #000000c7;"> G-Production</span>
                        </td>
                    </tr>
                    </table>
                </td>
                </tr>
            </table>
        ';
        $mailData['html'] = $this->template_email($content_html);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $mailData);
        $result = curl_exec($this->curl);
        curl_close($this->curl);
        return $result;
    }

    private function template_email($content)
    {
        return '
            <html>
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title>Bidan Kita</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                </head>
                <body style="margin: 0; padding: 0; width: fit-content; margin-left: auto; margin-right: auto; ">
                <table border="0" cellpadding="0" cellspacing="0"> 
                    <tr>
                    <td align="center">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style=" background: white; margin-top: -15px; padding-top: 25px;"></table>
                    </td>
                    </tr>
                    <tr>
                    <td>
                        ' . $content . '
                    </td>
                    </tr>
                </table>
                </body>
            </html>
        ';
    }
}
