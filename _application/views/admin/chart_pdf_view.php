<h1><?php echo $title; ?></h1>
<h2><?php echo $tanggal; ?></h2>
<table width="100%" class="table table-bordered">
	<thead>
		<tr>
			<td style="width:10%;border: .5px solid #000; font-size:14px;text-align: center;">No.</td>
			<td style="width:30%;border: .5px solid #000; font-size:14px;text-align: center;">TANGGAL</td>
			<td style="width:30%;border: .5px solid #000; font-size:14px;text-align: center;">WAKTU</td>
			<td style="width:30%;border: .5px solid #000; font-size:14px;text-align: center;"><?php echo strtoupper($type); ?></td>
		</tr>
	</thead>
	<tbody>
	<?php 
	$no = 1;
	foreach ($data as $key => $row) { ?>
		<tr>
			<td style="width:10%;border: .5px solid #000; font-size:10px;text-align: center;"><?php echo $no; ?></td>
			<td style="width:30%;border: .5px solid #000; font-size:10px;text-align: center;"><?php echo convert_date($row->tanggal, '', '', 'id'); ?></td>
			<td style="width:30%;border: .5px solid #000; font-size:10px;text-align: center;"><?php echo $row->jam; ?></td>
			<td style="width:30%;border: .5px solid #000; font-size:10px;text-align: center;"><?php echo $row->$type; ?></td>
		</tr>		
	<?php
	$no++;
	} ?>
	</tbody>
</table>